import express from 'express';
import { createAdmin, loginAdmin, listAllAdmin, listSingleAdmin } from '../controllers/admin';
import { createTransaction, listSingleTransaction, listAllTransaction } from '../controllers/transaction';
import verifyToken from '../auth/verifyToken';

const router = express.Router();

router.post('/admin/signup', createAdmin);
router.post('/admin/login', loginAdmin);
router.get('/admins', verifyToken, listAllAdmin);
router.get('/admins/:adminId', verifyToken, listSingleAdmin);
router.post('/transactions', verifyToken, createTransaction);
router.get('/transactions', verifyToken, listAllTransaction);
router.get('/transactions/:transactionId', verifyToken, listSingleTransaction);

export default router;
