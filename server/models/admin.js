import mongoose from 'mongoose';
mongoose.Promise = global.Promise;

const adminSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  }
});

export default mongoose.model('Admin', adminSchema);
