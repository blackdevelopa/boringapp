import mongoose from 'mongoose';
mongoose.Promise = global.Promise;

const transactionSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  item: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  customer: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  Admin: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Admin',
  }
});

export default mongoose.model('Transaction', transactionSchema);