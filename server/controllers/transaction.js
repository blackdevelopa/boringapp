import mongoose from 'mongoose';
import Transaction from '../models/transaction';
import Admin from '../models/admin';

export function createTransaction(req, res) {
  const transaction = new Transaction({
    _id: mongoose.Types.ObjectId(),
    item: req.body.item,
    quantity: req.body.quantity,
    price: req.body.price,
    customer: req.body.customer,
  });

  if (!transaction.item || !transaction.quantity || !transaction.price || !transaction.customer) {
    return res.status(400).json({ sucess: false, message: 'Ensure you filled item, quantity, price and customer'});
  }

  return transaction
    .save()
    .then((newTransaction) => res.status(201).json({ success: true, message: 'Transaction is created', newTransaction}))
    .catch(() => res.status(500).json({ success: false, message: 'Server is down'}));
}

export function listAllTransaction(req, res) {
  Transaction.find()
    .populate('Admin')
    .exec()
    .then((allTransaction) => res.status(200).json({ success: true, message: 'All Transactions', allTransaction}))
    .catch(() => res.status(500).json({ success: false, message: 'Server is down'}));
}

export function listSingleTransaction(req, res) {
  const id = req.params.transactionId;
  Transaction.findById(id)
    .populate('Admin')
    .then((singleTransaction) => res.status(200).json({ success: true, message: 'View transaction', singleTransaction}))
    .catch(() => res.status(500).json({ success: false, message: 'Server is down'}));
}