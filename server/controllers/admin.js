import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import createToken from '../auth/token';
import Admin from '../models/admin';

export function createAdmin(req, res) {
  bcrypt.hash(req.body.password, 10, (err, hash) => {
    const password = hash;
    const admin = new Admin({
      _id: mongoose.Types.ObjectId(),
      email: req.body.email,
      password,
    });

    if(!admin.email || !admin.password) {
      return res.status(400).json({ success: false, message: 'Fill an email and a password'})
    }

    return Admin.countDocuments({
      $or: [
        { email: req.body.email },
      ],
    })
    .then((count) => {
      if (count > 0) {
        return res.status(400).json({ success: false, message: 'This account exists'});
      }

      return admin
        .save()
        .then((newAdmin) => {
          const token = createToken(newAdmin);
          return res.status(201).json({ success: true, mesage: 'New Admin created', token})
        })
        .catch(() => res.status(500).json({ success: false, message: 'Server is down'}));
    });
  });
}

export function loginAdmin(req, res) {
  const { email, password } = req.body;
  Admin.findOne({ email })
    .then((regAdmin) => {
      bcrypt.compare(password, regAdmin.password, (err, result) => {
        if (err) {
          return res.status(401).json({success:false, message:'Unanuthorize Access'})
        }
        if (result) {
          const token = createToken(regAdmin);
          return res.status(200).json({success: true, message: 'Authentication successful', token});
        }
        return res.status(401).json({success: false, message: 'Wrong login details'});
      });
    })
    .catch((err) => res.status(500).json({ success: false, message: 'Server is down', erroe: err.mesage}));
}

export function listAllAdmin(req, res) {
  Admin.find()
    .then((allAdmin) => res.status(200).json({success: false, mesage: 'All Admins', allAdmin}))
    .catch(() => res.status(500).json({success: false, mesage: 'Server is down'}));
}

export function listSingleAdmin(req, res) {
  const id = req.params.adminId;
  Admin.findById(id)
    .then((OneAdmin) => res.status(500).json({success:true, mesage: 'An Admin detail', OneAdmin}))
    .catch(() => res.status(500).json({success: false, mesage: 'Server is down'}));
}