import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';
import cors from 'cors';
import mongoose from 'mongoose';
import routes from './routes/index';

const app = express();
const port = process.env.PORT || 8001;

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

mongoose.connect(process.env.DB, { useNewUrlParser: true})
  .then(() => {
    console.log('Database connected');
  })
  .catch((err) => {
    console.log('err', err)
  });
mongoose.Promise = global.Promise;


app.use('/api/v1', routes);

app.get('/', (req, res) => res.status(200).json({ success: true, message: 'Welcome to Corporate Hair Application'}));
app.get('*', (req, res) => res.status(404).send({ message: 'You lost?' }));

app.listen(port, () => {
  console.log('App running at ', port);
});

export default app;